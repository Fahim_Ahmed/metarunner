# MetaRunner
This is a prototype platformer game. This game features:

- Raycast based physics system. (Not Unity's rigidbody based physics.)
- Moving platforms that support multiple passengers movements standing on platform.
- Template based theming system.
- Homing missile system.
- GUI based level generator for debugging purpose.

This project has many extra functionalities which are not implemented in the game.

Play store link: [MetaRunner Android](https://play.google.com/store/apps/details?id=com.Undefined.MetaRunner)

You can download the binary file here: 

- [MetaRunner Windows](https://bitbucket.org/Fahim_Ahmed/metarunner/downloads/MetaRunner_v1.zip)
- Alternate link: [MetaRunner Google Drive](https://drive.google.com/open?id=1iLfqcH9F6_sWMgO6Dcb6jwm60d8L0373)
