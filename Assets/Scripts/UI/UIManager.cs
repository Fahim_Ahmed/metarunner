﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;
using MetaRunner.Controller;
using MetaRunner.Game;
using System;

namespace MetaRunner.UI {

    public class UIManager : MonoBehaviour {

        public GameObject missileUIElement;
        public TextMeshProUGUI score;
        public TextMeshProUGUI best_score;
        public GameObject menuPanel;
        public TextMeshProUGUI finalScoreText;

        MissileController missileController;
        GameManager gameManager;

        LevelManager levelManager;

        List<GameObject> missileElements;

        int maxMissiles;
        int missileCounter;

        void Start() {
            gameManager = FindObjectOfType<GameManager>();

            missileController = FindObjectOfType<MissileController>();
            maxMissiles = missileController.maxMissileCount;            

            levelManager = FindObjectOfType<LevelManager>();
            levelManager.onGameOver.AddListener(OnGameOver);

            GenerateMissileUI(maxMissiles);

            missileController.onMissileLaunch.AddListener(MissileFired);
            missileController.onMissileReloadComplete.AddListener(MissileReloaded);

            gameManager.LoadBestScore(best_score);

            #if UNITY_ANDROID
            transform.Find("Hint").gameObject.SetActive(false);
            #endif
        }

        void FixedUpdate(){
            UpdateScore();
        }

        void GenerateMissileUI(int maxMissile) {
            missileElements = new List<GameObject>();
            missileElements.Add(missileUIElement.transform.Find("missile").gameObject);

            for(int i = 1; i < maxMissile; i++){
                Vector3 p = missileUIElement.transform.position + Vector3.right * i * -36f;
                GameObject m = Instantiate(missileUIElement, p, Quaternion.identity, missileUIElement.transform.parent);
                missileElements.Add(m.transform.Find("missile").gameObject);
            }
        }

        void MissileFired() {
            missileElements[maxMissiles - missileCounter - 1].SetActive(false);
            missileCounter++;
        }

        void MissileReloaded() {
            missileCounter--;
            missileElements[maxMissiles - missileCounter - 1].SetActive(true);
        }

        void UpdateScore() {
            score.text = levelManager.score + "";
        }

         void OnGameOver() {
            finalScoreText.text = "Game Over!\nYour score: " + levelManager.score;
            gameManager.PauseGame();
            gameManager.SaveBestScore(levelManager.score, best_score);
        }

        internal void ActivateMenu(bool v){
            menuPanel.SetActive(v);
        }
    }
}