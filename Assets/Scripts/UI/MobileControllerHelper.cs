﻿using UnityEngine;

namespace MetaRunner.UI {

    public class MobileControllerHelper : MonoBehaviour {
        void Start(){
            #if UNITY_STANDALONE
            gameObject.SetActive(false);
            #endif            
        }
    }
}