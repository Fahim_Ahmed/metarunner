﻿using UnityEngine;
using MetaRunner.Helper;

namespace MetaRunner.Props {

    /// <summary>
    /// Base class for platform object.
    ///</summary>

    [CreateAssetMenu(fileName = "New Platform", menuName = "Game Helpers/Platform")]
    public class PlatformTemplate : ScriptableObject {
        public const float baseSize = GameUnits.baseUnit;
        public const float maxSize = GameUnits.actualMaxWidth;
        
        [SerializeField]
        Vector2 size = new Vector2(1,1);
        
        public bool passable = true;
        public bool destroyable = false;

        [Range(-5, 5)]
        public int positionTopDown = 0;
        
        [Range(0,11)]
        public int positionLeftRight = 0;
        public bool isFreeSpawn = false;

        public Vector2 GetSize(){
            Vector2 s = new Vector2(Mathf.Round(size.x), Mathf.Round(size.y));
            return s * baseSize;
        }

         public void SetSize(Vector2 v){
            size = v;
        }
    }
}