﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using MetaRunner.Props;
using MetaRunner.Game;

namespace MetaRunner.Helper {

    public class ModuleGeneratorEditor : EditorWindow {

        /// <summary>
        /// Just a helper GUI module to generate platforms.
        /// </summary>

        GameObject defaultPlatform;
        ModuleGenerator moduleGenerator;

        int numberOfPlatform = 1;
        int numberOfModule;

        int minSizeX = 1;
        int maxSizeX = 1;

        const float baseUnit = GameUnits.baseUnit;
        const float maxModuleWidth = GameUnits.actualMaxWidth;
        
        List<GameObject> platforms = new List<GameObject>();
        Dictionary<GameObject, int[]> platformsX = new Dictionary<GameObject, int[]>();
        Dictionary<GameObject, int> platformsY = new Dictionary<GameObject, int>();

        List<GameObject> modules = new List<GameObject>();

        [MenuItem("Tools/Module Generator")]
        public static void ShowWindows(){
            GetWindow<ModuleGeneratorEditor>("Module Generator");            
        }

        void OnGUI() {
            defaultPlatform = (GameObject) EditorGUILayout.ObjectField("Default platform object", defaultPlatform, typeof(GameObject), true);
            numberOfPlatform = EditorGUILayout.IntSlider("Number of platforms", numberOfPlatform, 1, 20);
            minSizeX = EditorGUILayout.IntSlider("Min platform size", minSizeX, 1, 12);
            maxSizeX = EditorGUILayout.IntSlider("Max platform size", maxSizeX, 1, 12);

            if(GUILayout.Button("Generate Module")){
                GeneratePlatforms();
            }

            if(GUILayout.Button("Save as prefab")){
                Debug.Log("Saved");
            }

            GUILayout.Label("Generate whole level");
            numberOfModule = EditorGUILayout.IntSlider("Number of modules", numberOfModule, 1, 20);
            if(GUILayout.Button("Generate Level")){
                GenerateLevel();
            }
        }

        GameObject GeneratePlatforms(int offsetX = 0){
            moduleGenerator = new ModuleGenerator(defaultPlatform, numberOfPlatform, minSizeX, maxSizeX);
            GameObject parent = moduleGenerator.GeneratePlatforms(offsetX);

            return parent;
        }

        void GenerateLevel(){
            modules = new List<GameObject>();

            for (int i = 0; i < numberOfModule; i++){                
                modules.Add(GeneratePlatforms(i));
            }
        }
    }
}