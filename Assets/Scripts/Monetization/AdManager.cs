﻿using UnityEngine;
using  UnityEngine.Advertisements;

namespace MetaRunner.Ad {

    public class AdManager : MonoBehaviour {

        #if UNITY_ANDROID

        string gameId = "3415629";
        public bool testMode = false;

        internal static int retryCount;
        
        void Start() {
            retryCount = 0;
            Advertisement.Initialize(gameId, testMode);
        }

        void Update(){
            if(retryCount == 3){
                Advertisement.Show();
                retryCount = 0;
            }
        }

        #endif
    }
}