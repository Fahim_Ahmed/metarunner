﻿using System.Collections.Generic;
using UnityEngine;

using MetaRunner.Props;
using MetaRunner.Helper;
using MetaRunner.Controller;

namespace MetaRunner.Game {

    public class ModuleGenerator {
        public GameObject defaultPlatform;
        public int numberOfPlatform = 1;
        public int minSizeX = 1;
        public int maxSizeX = 1;

        List<GameObject> platforms;
        Dictionary<GameObject, int[]> platformsX;
        Dictionary<GameObject, int> platformsY;
        List<GameObject> modules = new List<GameObject>();

        public ModuleGenerator(GameObject g, int numberOfPlatoform, int minSizeX, int maxSizeX){
            this.defaultPlatform = g;
            this.numberOfPlatform = numberOfPlatoform;
            this.minSizeX = minSizeX;
            this.maxSizeX = maxSizeX;
        }

        public GameObject GeneratePlatforms(int offsetX = 0, float enemySpawnChance = 0.1f){
            platforms = new List<GameObject>();
            platformsX = new Dictionary<GameObject, int[]>();
            platformsY = new Dictionary<GameObject, int>();
            
            GameObject parent = new GameObject("Module");
            Vector2 pos = parent.transform.position;
            pos.x = offsetX * GameUnits.actualMaxWidth;
            parent.transform.position = pos;

            for(int i = 0; i < numberOfPlatform; i++){
                int sizeX = Random.Range(minSizeX, maxSizeX);
                
                int posX = Random.Range(0, 12);
                posX = (posX + sizeX >= GameUnits.maxWidthOfEachModule) ? (GameUnits.maxWidthOfEachModule - sizeX) : posX;

                int[] ySet = new int[4]{4, 1, -2, -5};
                int posY = Random.Range(0, 4);
                posY = ySet[posY];

                if(i > 0 && IsPlatformExist(posX, posY, sizeX)){
                    if(i == numberOfPlatform - 1 && platforms.Count > 1){
                        i--;                        
                    }
                    continue;
                }

                GameObject p = SpawnPlatform(posX, posY, sizeX, parent.transform, enemySpawnChance);
                
                // p.GetComponentInChildren<EnemyController>().DisableEnemy(spawnChance);
                
                platforms.Add(p);

                int[] inX = new int[sizeX];
                for (int j = 0; j < sizeX; j++){ inX[j] = posX + j; }
                platformsX.Add(p, inX);
                platformsY.Add(p, posY);
            }

            SolveMinDistanceBetweenPlatforms(ref platforms);

            return parent;
        }

        // spwan at any location
        // public GameObject SpawnPlatform(float posX, float posY, int sizeX, float enemySpawnChance){
        //     GameObject p = GameObject.Instantiate(defaultPlatform);
        //     SyncPlatformSettings syncer = p.GetComponent<SyncPlatformSettings>();            
        //     // p.tag = "Through";
            
        //     // p.transform.position = new Vector3(posX, posY, 0);
        //     // p.GetComponent<SpriteRenderer>().size = new Vector3(GameUnits.baseUnit * sizeX, GameUnits.baseUnit);
                        
        //     PlatformTemplate platformTemplate = GameObject.Instantiate(syncer.platform);
        //     platformTemplate.name = "ExtraPlatformTemplate";
        //     platformTemplate.SetSize(new Vector2(sizeX, 1));
        //     platformTemplate.positionLeftRight = (int) posX;
        //     platformTemplate.positionTopDown = (int) posY;
        //     platformTemplate.passable = true;
        //     platformTemplate.isFreeSpawn = true;
            
        //     syncer.platform = platformTemplate;
        //     syncer.Start();

        //     bool spawnChance = (Random.Range(0, 1f) >= enemySpawnChance);
        //     syncer.enemyObject.SetActive(spawnChance);

        //     return p;
        // }

        public GameObject SpawnPlatform(int posX, int posY, int sizeX, Transform parent, float enemySpawnChance) {
            if(parent == null) parent = new GameObject("Extra platform").transform;
            GameObject p = GameObject.Instantiate(defaultPlatform, parent);
            SyncPlatformSettings syncer = p.GetComponent<SyncPlatformSettings>();
            
            PlatformTemplate platformTemplate = GameObject.Instantiate(syncer.platform);
            platformTemplate.name = "PlatformTemplate";
            platformTemplate.SetSize(new Vector2(sizeX, 1));
            platformTemplate.positionLeftRight = posX;
            platformTemplate.positionTopDown = posY;
            platformTemplate.passable = true;
            
            syncer.platform = platformTemplate;
            syncer.Start();

            bool spawnChance = (Random.Range(0, 1f) <= enemySpawnChance);
            syncer.enemyObject.SetActive(spawnChance);
            
            return p;
        }

        void SolveMinDistanceBetweenPlatforms(ref List<GameObject> plats){
            int minDst = 300;
            int idx = 0;

            for (int i = 0; i < platforms.Count; i++) {
                int A = plats[i].GetComponent<SyncPlatformSettings>().platform.positionTopDown;

                for (int j = i+1; j < platforms.Count; j++){
                    int B = plats[j].GetComponent<SyncPlatformSettings>().platform.positionTopDown;

                    int d = Mathf.Abs(A-B);
                    if(d < minDst){
                        minDst = d;
                        idx = i;
                    }
                }
            }

            int distThreshold = 6;
            if(minDst > distThreshold && minDst != 300){
                int p = plats[idx].GetComponent<SyncPlatformSettings>().platform.positionTopDown;
                int diff = (minDst - distThreshold);

                plats[idx].name = minDst + "_mark";

                plats[idx].GetComponent<SyncPlatformSettings>().platform.positionTopDown = p < 0 ? p + diff : p - diff;
            }
        }

        bool IsPlatformExist(int pX, int pY, int sX){
            int[] inX = new int[sX];
            for (int j = 0; j < sX; j++){ 
                inX[j] = pX + j; 
            }

            for (int i = 0; i < platforms.Count; i++){
                int[] ix = platformsX[platforms[i]];
                int iy = platformsY[platforms[i]];

                if(pY != iy) continue;

                for (int k = 0; k < inX.Length; k++){
                    for (int j = 0; j < ix.Length; j++){
                        if(ix[j] == inX[k]){
                            return true;
                        }                        
                    }
                }
            }

            return false;
        }

        int GetRandomYPos(int lastY){
            switch(lastY){
                case 0: return 1;
                case 1: return Random.Range(0,1f) >= 0.5f ? 2 : 0;
                case 2: return Random.Range(0,1f) >= 0.5f ? 1 : 3;
                case 3: return 2;
            }

            return 0;                        
        }
    }
}