﻿using UnityEngine;
using MetaRunner.UI;
using MetaRunner.Inputs;
using TMPro;

namespace MetaRunner.Game {

    /// <summary>
    /// Save and load best score.
    /// Start and pause game.
    /// </summary>
    public class GameManager : MonoBehaviour {

        UIManager ui;
        AudioSource destroyerSound;

        const string best_score_key = "best_score";

        void Start() {
            destroyerSound = FindObjectOfType<MetaRunner.Controller.DestroyerController>().GetComponent<AudioSource>();
            ui = FindObjectOfType<UIManager>();
            PauseGame();
        }

        public void StartGame() {
            Time.timeScale = 1f;
            destroyerSound.mute = false;
            // AudioListener.volume = 1f;
            ui.ActivateMenu(false);
            PlayerInput.pauseGame = false;

            GetComponent<LevelManager>().ResetGame();
        }

        public void PauseGame() {
            Time.timeScale = 0f;
            destroyerSound.mute = true;
            ui.ActivateMenu(true);
            PlayerInput.pauseGame = true;
        }

        internal void LoadBestScore(TextMeshProUGUI txt){
            int best_score = PlayerPrefs.GetInt(best_score_key, 0);
            txt.text = "Best: " + best_score;
        }

        internal void SaveBestScore(int score, TextMeshProUGUI txt) {            
            int best_score = PlayerPrefs.GetInt(best_score_key, 0);
            if(score > best_score){
                PlayerPrefs.SetInt(best_score_key, score);
                txt.text = "Best: " + score;
            }
        }
    }
}