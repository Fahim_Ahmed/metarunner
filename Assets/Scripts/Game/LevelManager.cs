﻿using UnityEngine;
using UnityEngine.Events;

using MetaRunner.Character;
using MetaRunner.Controller;

namespace MetaRunner.Game {

    /// <summary>
    /// Generate platforms.
    /// </summary>

    public class LevelManager : MonoBehaviour {

        // fixed unit from player size. 1 unit = 0.675f. 
        const float baseUnit = 0.675f;
        const float baseSizeOfOneModule = 12f * baseUnit;
        
        // Generate a level after crossing this amount of distance
        float levelGenerateThreshold = baseSizeOfOneModule / 3;
        float checkDistanceOffset = baseSizeOfOneModule;
        
        int chunkCount = 1;

        // generate a new chunk after crossing this point + threshold
        float nextTravelPoint = 0;

        public GameObject defaultPlatform;
        public float enemySpawnChance = 0.1f;
        
        ModuleGenerator moduleGenerator;
        
        public int numberOfChunkToGenerateAhead = 3;
        public int numberOfPlatform = 4;
        
        int minSizeX = 3;
        int maxSizeX = 8;

        new Camera camera;
        GameObject modulesParent;
        
        DestroyerController destroyer;
        
        AudioSource audioSource;

        Player player;
        Vector3 startingPosition;

        public int score;
        public int extraScore;

        [HideInInspector]
        public UnityEvent onGameOver;

        float travelledDistance;
        bool reachedDiffThreshold = false;
        bool isDead = false;
        
        void Awake() {
            onGameOver = new UnityEvent();
        }
        
        void Start() {
            Application.targetFrameRate = 120;

            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            player.onHitWithEnemy.AddListener(GameOver);
            startingPosition = player.transform.position;

            destroyer = FindObjectOfType<DestroyerController>();
            if(destroyer != null) destroyer.onDestroyerHit.AddListener(GameOver);

            camera = Camera.main;
            moduleGenerator = new ModuleGenerator(defaultPlatform, numberOfPlatform, minSizeX, maxSizeX);

            modulesParent = new GameObject("Modules Parent");

            audioSource = GetComponent<AudioSource>();
        }

        void OnDrawGizmos(){
            Vector2 start = new Vector2(nextTravelPoint, -1); 

            Gizmos.color =  Color.cyan;
            Gizmos.DrawWireSphere(start, 0.25f);
            Gizmos.DrawWireSphere(start + Vector2.right * levelGenerateThreshold, 0.25f);
        }

        void Update() {
            // scoring
            if(player.velocity.x != 0){
                travelledDistance = Vector3.Distance(player.transform.position, startingPosition);
                score = Mathf.RoundToInt(travelledDistance) + extraScore;
            }

            if(!reachedDiffThreshold && score > 200) {
                destroyer.speed += 2;
                moduleGenerator.numberOfPlatform = 3;
                enemySpawnChance = 0.25f;
                reachedDiffThreshold = true;
            }

            // game over conditions
            if(player.transform.position.y < -12f && !isDead) GameOver();

            GenerateLevel();
        }

        void GameOver(){
            Debug.Log("Game over. " + score);
            audioSource.Play();
            onGameOver.Invoke();
            isDead = true;

            // ResetGame();
            #if UNITY_ANDROID
            MetaRunner.Ad.AdManager.retryCount++;
            #endif
        }

        public void ResetGame(){
            player.transform.position = startingPosition;

            SpriteRenderer[] srs = modulesParent.GetComponentsInChildren<SpriteRenderer>();
            foreach(var sr in srs) Destroy(sr.gameObject);
            
            chunkCount = 1;
            nextTravelPoint = 0;
            travelledDistance = 0;
            score = 0;
            extraScore = 0;
            isDead = false;

            destroyer.transform.position = Vector3.right * -8f;

            Camera.main.transform.position = Vector3.zero;
            GetComponent<GameThemeManager>().NewTheme();

            reachedDiffThreshold = false;
            destroyer.speed = 4;
            moduleGenerator.numberOfPlatform = 4;
            enemySpawnChance = 0.1f;            
        }

        void GenerateLevel(){
            // generate module ahead
            if(camera.transform.position.x - nextTravelPoint + checkDistanceOffset >= levelGenerateThreshold){
                for(int i = 0; i < numberOfChunkToGenerateAhead; i++){
                    // call generator method here.
                    GameObject moduleRoot = moduleGenerator.GeneratePlatforms(chunkCount, enemySpawnChance);
                    moduleRoot.transform.parent = modulesParent.transform;
                    chunkCount++;
                }

                nextTravelPoint += baseSizeOfOneModule * numberOfChunkToGenerateAhead;
            }
        }
    }
}