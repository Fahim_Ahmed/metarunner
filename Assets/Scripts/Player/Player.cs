﻿using UnityEngine;
using UnityEngine.Events;

using MetaRunner.Controller;
using MetaRunner.Game;
using System;

namespace MetaRunner.Character {

    /// <summary>Main player class</summary>
    [RequireComponent(typeof(PlayerController))]
    public class Player : MonoBehaviour {

        public float minJumpHeight = 1f;
        public float maxJumpHeight = 4f;
        
        /// <summary>Time required to reach the max jump height</summary>
        public float timeToJumpApex = 0.4f;

        public float moveSpeed = 6f;
        
        public float smoothTimeAirborne = 0.2f;
        public float smoothTimeGrounded = 0.1f;

        public float morphingThreshold = 1;
        public int platformGenerateOffsetY = 1;

        public Transform spotLightTransform;

        SkinnedMeshRenderer skinRenderer;

        float gravity;
        float minJumpVelocity;
        float maxJumpVelocity;

        internal Vector2 velocity;
        
        [HideInInspector]
        PlayerController playerController;
        MissileController missileController;

        /// <summary>ref.false variable for movement smoothing.</summary>
        float velocityXSmoothing;
        Vector2 directionalInput;

        LevelManager levelManager;

        [HideInInspector]
        public UnityEvent onHitWithEnemy;

        void Awake(){
            onHitWithEnemy = new UnityEvent();
        }

        void Start() {
            playerController = GetComponent<PlayerController>();
            missileController = GetComponent<MissileController>();
            skinRenderer = GetComponent<SkinnedMeshRenderer>();

            levelManager = FindObjectOfType<LevelManager>();

            // u = initial velocity, a = acceleration, t = time, s = distance
            //Formula to calculate gravity: s = ut + 0.5at
            gravity = -1 * (2 * maxJumpHeight / Mathf.Pow(timeToJumpApex, 2));

            //Max jump velocity from gravity and time to reach max height: v = u + at
            maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
            
            //v^2 = u^2 + 2as
            minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);

            Debug.Log("Gravity: " + gravity);
            Debug.Log("Min jump Velocity: " + minJumpVelocity);
            Debug.Log("Max jump Velocity: " + maxJumpVelocity);
        }

        internal void SetDirectionalInput(Vector2 input){
            directionalInput = input;
        }

        internal void OnJumpDown(){
            if (playerController.collisionInfo.below) {
                velocity.y = maxJumpVelocity;
            }
        }

        internal void OnJumpUp(){
            // variable jump height. If you release the jump key before reaching max height, clip the velocity with minJumpVelocity
            if(velocity.y > minJumpHeight) {
                velocity.y = minJumpHeight;                
            }
        }

        internal void OnLaunchMissile() {
            missileController.LaunchMissile();
        }

        void Update() {
            CalculateVelocity();

            playerController.Move(velocity * Time.deltaTime, directionalInput);

            // Fix a issue when player in on a moving platform.
            if (playerController.collisionInfo.above || playerController.collisionInfo.below) {
                velocity.y = 0;
            }

            if(velocity.y != 0){
                Vector3 p = Vector2.right * (velocity.x * Time.deltaTime + transform.position.x);
                Debug.DrawLine(p, new Vector3(p.x, 0.25f, 0), Color.cyan);
            }

            SpotLightMovement();
            MorphShape(velocity.x);
        }

        void SpotLightMovement() {
            if(spotLightTransform == null) return;
            float angle = RemapValue(velocity.x, -8f, 8f, 80f, 100f, true);
            spotLightTransform.rotation = Quaternion.Euler(0, 0, -angle);
        }

        void CalculateVelocity() {
            float targetVelocityX = directionalInput.x * moveSpeed;
            float smoothTime = playerController.collisionInfo.below ? smoothTimeGrounded : smoothTimeAirborne;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, smoothTime);
            velocity.y += gravity * Time.deltaTime;
        }

        // Change shape based on speed.
        void MorphShape(float speedX) {
            float transformRange = moveSpeed; //morphingThreshold + 5;
            speedX = Mathf.Abs(speedX);

            float blendVal = RemapValue(speedX, morphingThreshold, transformRange, 0, 100f, true);
            skinRenderer.SetBlendShapeWeight(0, blendVal);
        }

        #region triggers
        void OnTriggerEnter2D(Collider2D col) {
            if(col.gameObject.tag == "Enemy"){
                onHitWithEnemy.Invoke();
            }
        }

        #endregion

        #region utility methods

        // Remap value from one range to another.
        float RemapValue(float value, float start1, float stop1, float start2, float stop2, bool clamp = false) {
            float v = start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));

            if (clamp) {
                if (v < start2) v = start2;
                if (v > stop2) v = stop2;
            }

            return v;
        }

        #endregion
    }
}