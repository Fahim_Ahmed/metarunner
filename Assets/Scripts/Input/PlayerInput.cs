﻿using UnityEngine;
using MetaRunner.Character;
using UnityStandardAssets.CrossPlatformInput;

namespace MetaRunner.Inputs {

    [RequireComponent(typeof(Player))]
    public class PlayerInput : MonoBehaviour {        
        public static bool pauseGame;
        
        public AudioClip fireTune;

        Player player;
        AudioSource jumpAudioSource;

        void Start() {
            player = GetComponent<Player>();
            jumpAudioSource = GetComponent<AudioSource>();
        }

        void Update() {
            if(!pauseGame){
                #if UNITY_EDITOR || UNITY_STANDALONE
                
                Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
                // directionalInput.x = (Input.GetKey(KeyCode.D)) ? 1.0f : 0f;
                player.SetDirectionalInput(directionalInput);

                if(Input.GetKeyDown(KeyCode.Space)){
                    jumpAudioSource.Play();
                    player.OnJumpDown();
                }

                if(Input.GetKeyUp(KeyCode.Space)){
                    player.OnJumpUp();
                }

                if(Input.GetMouseButtonUp(0)){
                    player.OnLaunchMissile();
                }
                
                #endif

                #if UNITY_ANDROID && !UNITY_EDITOR

                // Vector2 directionalInput = new Vector2(CrossPlatformInputManager.GetAxisRaw("Horizontal"), CrossPlatformInputManager.GetAxisRaw("Vertical"));

                if(CrossPlatformInputManager.GetButtonDown("Jump")){
                    jumpAudioSource.Play();
                    player.OnJumpDown();
                }

                if(CrossPlatformInputManager.GetButtonUp("Jump")){
                    player.OnJumpUp();
                }

                if(CrossPlatformInputManager.GetButtonUp("Fire")){
                    player.OnLaunchMissile();
                }

                Vector2 directionalInput = new Vector2(0, 0);

                if(CrossPlatformInputManager.GetButtonUp("Drop")){
                    directionalInput.y = -1f;
                }

                if(CrossPlatformInputManager.GetButton("Move")){
                    directionalInput.x = 1f;
                }
                

                player.SetDirectionalInput(directionalInput);

                #endif

                if(Input.GetKeyUp(KeyCode.Escape)) Application.Quit();
            }

            // if(Input.GetKeyUp(KeyCode.P)){
            //     PauseGame(!pauseGame);
            // }
        }

        // void PauseGame(bool pause) {
        //     Time.timeScale = pause ? 0 : 1;
        //     pauseGame = pause;
        // }
    }
}