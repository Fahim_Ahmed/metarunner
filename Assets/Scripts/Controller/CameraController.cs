﻿using UnityEngine;
using MetaRunner.Character;

namespace MetaRunner.Controller {
    /// <summary>
    /// Camera controller. Smoothly follow the player using a focus box.
    /// </summary>
    public class CameraController : MonoBehaviour {
        
        public PlayerController target;
        
        // Deadzone box. Camera will not move when target stays in this area.
        public Vector2 focusAreaSize;

        // adjust vertical position of camera.
        public float verticalOffset = 1;
        
        // how much camera will move ahead when player will start moving in any direction.
        public float lookAheadDstX = 4;
                
        public float lookSmoothTimeX = 0.4f;
        public float verticalSmoothTime = 0.1f;

        // this is the circular gameobject behind the player.
        public Transform focus;

        // ** helper variables for calculating the camera ahead look
        float currentLookAheadX;
        float targetLookAheadX;
        float lookAheadDirX;
        float smoothLookVelocityX;
        float smoothVelocityY;
        Vector2 smoothVelocityFocus;
        bool lookAheadStopped;
        
        // **
        
        FocusArea focusArea;

        void Start(){
            if(target == null) target = FindObjectOfType<PlayerController>();
            focusArea = new FocusArea(target.collider.bounds, focusAreaSize);            
        }

        void LateUpdate(){
            // update focus area bounds
            focusArea.Update(target.collider.bounds);
            
            SmoothFollowCamera();
        }

        void SmoothFollowCamera(){
            Vector2 focusPosition = focusArea.center + Vector2.up * verticalOffset;

            if(focusArea.velocity.x != 0){ 
                lookAheadDirX = Mathf.Sign(focusArea.velocity.x);

                if(lookAheadDirX == Mathf.Sign(target.playerInput.x) && target.playerInput.x != 0){
                    lookAheadStopped = false;
                    targetLookAheadX = lookAheadDirX * lookAheadDstX;
                }
                else {
                    if(!lookAheadStopped){
                        lookAheadStopped = true;
                        targetLookAheadX = currentLookAheadX + (lookAheadDirX * lookAheadDstX - currentLookAheadX) * 0.25f;
                    }
                }
            }
            
            currentLookAheadX = Mathf.SmoothDamp(currentLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTimeX);
            
            focusPosition.y = Mathf.SmoothDamp(transform.position.y, focusPosition.y, ref smoothVelocityY, verticalSmoothTime);
            focusPosition += Vector2.right * currentLookAheadX;

            transform.position = (Vector3) focusPosition + Vector3.forward * -10f;

            // set focus gameobject position
            Vector2 fp = focus.position;
            fp = Vector2.SmoothDamp(fp, new Vector2(focusArea.center.x, target.transform.position.y), ref smoothVelocityFocus, 0.2f);
            focus.position = fp;//new Vector3(focusArea.center.x, focusPosition.y, 0);
        }        

        void OnDrawGizmos(){
            Gizmos.color = new Color(1f, 0, 0, 0.1f);
            Gizmos.DrawCube(focusArea.center, focusAreaSize);

            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(transform.position + Vector3.forward * 10f, Vector3.one * 12f * 0.675f);
        }

        // Contains the focus box information and update bounds with respect to target's position.
        // Focus box will only move when target will hit the edge of the box.
        struct FocusArea {
            public Vector2 center;
            public Vector2 velocity;
            float left, right;
            float top, bottom;

            public FocusArea(Bounds targetBounds, Vector2 size){
                left = targetBounds.center.x - size.x * 0.5f;
                right = targetBounds.center.x + size.x * 0.5f;
                bottom = targetBounds.min.y;
                top = targetBounds.min.y + size.y;

                velocity = Vector2.zero;
                center = new Vector2((left + right) * 0.5f, (top + bottom) * 0.5f);
            }

            public void Update(Bounds targetBounds){
                float shiftX = 0;

                if(targetBounds.min.x < left) shiftX = targetBounds.min.x - left;
                if(targetBounds.max.x > right) shiftX = targetBounds.max.x - right;

                left += shiftX;
                right += shiftX;

                float shiftY = 0;

                if(targetBounds.min.y < bottom) shiftY = targetBounds.min.y - bottom;
                if(targetBounds.max.y > top) shiftY = targetBounds.max.y - top;

                top += shiftY;
                bottom += shiftY;

                center = new Vector2((left + right) * 0.5f, (top + bottom) * 0.5f);
                velocity = new Vector2(shiftX, shiftY);
            }
        }
    }
}