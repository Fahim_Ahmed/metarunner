﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MetaRunner.Controller {

    /// <summary>
    /// Launches missile and calculates it tragectory toward the target.
    /// Auto pick the nearest target and only shoot the target in front of the player.
    /// </summary>

    public class MissileController : MonoBehaviour {

        // Main missile gameobject
        public Transform missileSeed;
        
        // speed of the missile
        public float speed = 1.0f;
        public float ease = 1.0f;
        
        // how it'll survive in frames.
        public int lifeTime = 300;
        
        // max travel distance
        public float maxDistance = 15f;
        
        // how it will take to recharge on missile in second.
        public float reloadTime = 2f;
        
        // max missile count
        public int maxMissileCount = 3;

        Vector3 smoothedVelocity;
        
        // Missile pool.
        Queue<Transform> missiles;
        
        bool isReloading = false;

        [HideInInspector]
        public UnityEvent onMissileLaunch;
        
        [HideInInspector]
        public UnityEvent onMissileReloadComplete;        

        void Awake() {
            // creating missiles pool
            missiles = new Queue<Transform>();
            for (int i = 0; i < maxMissileCount; i++){
                ReloadMissile();
            }

            onMissileLaunch = new UnityEvent();
            onMissileReloadComplete = new UnityEvent();
        }

        void Update(){
            if(!isReloading && missiles.Count < maxMissileCount){
                isReloading = true;
                Invoke("ReloadMissile", reloadTime);
            }
        }

        void ReloadMissile(){
            Transform t = Instantiate(missileSeed, transform.localPosition, Quaternion.identity).transform;
            t.gameObject.SetActive(false);
            missiles.Enqueue(t);
            isReloading = false;

            onMissileReloadComplete.Invoke();
        }

        internal int getLoadedMissileCount(){
            return missiles.Count;
        }

        internal void LaunchMissile() {
            Transform target = FindClosestTarget();

            // TODO: pops status messages
            if(target == null || missiles.Count == 0) return;
            
            Transform missile = missiles.Dequeue();
            missile.position = transform.position + (Vector3.up * 0.4f);
            missile.gameObject.SetActive(true);

            // if out of range, reduce the life time.
            StartCoroutine(MoveMissile(missile, target, (target.name == "OOR") ? 30 : lifeTime));
            missile.GetComponent<AudioSource>().Play();
            onMissileLaunch.Invoke();
        }

        Transform FindClosestTarget() {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            float minD = 10000000f;
            Transform t = null;

            foreach(var e in enemies){
                if(e.transform.position.x < transform.position.x) continue;

                float dist = Vector3.Distance(transform.position, e.transform.position);
                if(dist <= minD){
                    minD = dist;
                    t = e.transform;
                    Debug.Log("tooooo far.");
                }
            }

            if(minD > maxDistance && t != null){
                t.name = "OOR";
            }

            return t;
        }

        IEnumerator MoveMissile(Transform missile, Transform target, int life){
            int counter = 0;
            
            while(true && missile != null){
                if(target == null){
                    DestroyMissile(missile);
                    break;
                }

                Vector3 lookDir = target.position - missile.position;
                Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, lookDir.normalized);
                
                missile.localRotation = Quaternion.Slerp(missile.rotation, targetRotation, ease);
                missile.Translate(missile.up.normalized * speed, Space.World);
                
                yield return new WaitForSeconds(0.0167f);
                counter++;

                if(counter == life){
                    DestroyMissile(missile);
                    break;
                }
            }
        }

        void DestroyMissile(Transform m){
            if(m != null) Destroy(m.gameObject);
        }
    }
}