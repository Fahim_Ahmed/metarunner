﻿using UnityEngine;

namespace MetaRunner.Controller {

    /// <summary>
    /// Player movements and collisions controlling class.
    /// This game uses raycasting based physics system because of greater flexibility on Player's movement control.
    /// </summary>
    public class PlayerController : RaycastController {

        /// Slope climbing threshold.
        public float maxClimbingAngle = 60f;
        public float maxDescendAngle = 60f;

        [HideInInspector]
        public CollisionInfo collisionInfo;

        Vector2 moveAmountOld;
        
        [HideInInspector]
        public Vector2 playerInput;

        public override void Start() {
            base.Start();
        }

        #region movement and collisions

        public void Move(Vector2 moveAmount, bool standingOnPlatform) {
            Move(moveAmount, Vector2.zero, standingOnPlatform);
        }

        public void Move(Vector2 moveAmount, Vector2 input, bool standingOnPlatform = false) {
            // update ray origins
            UpdateRaycastOrigins();

            // collision codes
            collisionInfo.Reset();
            moveAmountOld = moveAmount;
            
            playerInput = input;

            if(moveAmount.y < 0) DescendSlope(ref moveAmount);

            if(moveAmount.x != 0) HorizontalCollision(ref moveAmount);
            if(moveAmount.y != 0) VerticalCollision(ref moveAmount);

            // movements            
            transform.Translate(moveAmount);

            if(standingOnPlatform) collisionInfo.below = true;
        }

        void HorizontalCollision(ref Vector2 moveAmount) {
            float directionX = Mathf.Sign(moveAmount.x);
            float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;

            for (int i = 0; i < horizontalRayCount; i++) {
                Vector2 rayOrigin = directionX == -1 ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);

                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, directionX * Vector2.right, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

                if (hit) {

                    // check when collision object is in front or behind the player.
                    if(hit.distance == 0) continue;

                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

                    if (i == 0 && slopeAngle < maxClimbingAngle) {

                        // fix speed slow-down on crossed slopes
                        if (collisionInfo.descendingSlope) {
                            collisionInfo.descendingSlope = false;
                            moveAmount = moveAmountOld;
                        }

                        float distanceToSlopeStart = 0;
                        if(slopeAngle != collisionInfo.slopeAngleOld) {
                            distanceToSlopeStart = hit.distance - skinWidth;
                            moveAmount.x -= distanceToSlopeStart * directionX;
                        }

                        ClimbSlope(ref moveAmount, slopeAngle);
                        moveAmount.x += distanceToSlopeStart * directionX;
                    }

                    if (!collisionInfo.climbingSlope || slopeAngle > maxClimbingAngle) {
                        moveAmount.x = (hit.distance - skinWidth) * directionX;
                        rayLength = hit.distance;

                        if (collisionInfo.climbingSlope) {
                            moveAmount.y = Mathf.Tan(collisionInfo.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);
                        }

                        collisionInfo.left = directionX == -1;
                        collisionInfo.right = directionX == 1;
                    }
                }
            }
        }

        void VerticalCollision(ref Vector2 moveAmount) {
            float directionY = Mathf.Sign(moveAmount.y);
            float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;

            for (int i = 0; i < verticalRayCount; i++) {
                Vector2 rayOrigin = directionY == -1 ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);

                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, directionY * Vector2.up, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

                if (hit) {

                    if(hit.collider.tag == "Through"){
                        // pass thorugh ceiling on jump
                        if(directionY == 1 || hit.distance == 0) continue;
                        
                        // fix falling th for moving platform
                        if(collisionInfo.fallingThroughPlatform){
                            continue;
                        }

                        // jump down from platform
                        if(playerInput.y == -1){ 
                            collisionInfo.fallingThroughPlatform = true;
                            Invoke("ResetFallingThroughPlatform", 0.1f);
                            continue;
                        }
                    }

                    moveAmount.y = (hit.distance - skinWidth) * directionY;
                    rayLength = hit.distance;

                    if (collisionInfo.climbingSlope) {
                        moveAmount.x = moveAmount.y / Mathf.Tan(collisionInfo.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
                    }

                    collisionInfo.below = directionY == -1;
                    collisionInfo.above = directionY == 1;
                }
            }

            if (collisionInfo.climbingSlope) {
                float directionX = Mathf.Sign(moveAmount.x);
                rayLength = Mathf.Abs(moveAmount.x) + skinWidth;
                Vector2 rayOrigins = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + (moveAmount.y * Vector2.up);

                RaycastHit2D hit = Physics2D.Raycast(rayOrigins, Vector2.right * directionX, rayLength, collisionMask);
                if (hit) {
                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    if (slopeAngle != collisionInfo.slopeAngle) {
                        moveAmount.x = (hit.distance - skinWidth) * directionX;
                        collisionInfo.slopeAngle = slopeAngle;
                    }
                }
            }
        }

        void ClimbSlope(ref Vector2 moveAmount, float slopeAngle) {
            float moveDistance = Mathf.Abs(moveAmount.x);
            float climbmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

            // in the air, jumping check.
            if (moveAmount.y < climbmoveAmountY) {
                moveAmount.y = climbmoveAmountY;
                moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                
                collisionInfo.below = true;
                collisionInfo.climbingSlope = true;
                collisionInfo.slopeAngle = slopeAngle;
            }
        }

        void DescendSlope(ref Vector2 moveAmount) {
            float directionX = Mathf.Sign(moveAmount.x);
            Vector2 rayOrigin = ((directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft);

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

            if (hit) {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if(slopeAngle <= maxDescendAngle && slopeAngle != 0) {
                    if(Mathf.Sign(hit.normal.x) == directionX) {
                        if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x)) {
                            // calculate descend moveAmount
                            float moveDistance = Mathf.Abs(moveAmount.x);
                            float descmoveAmount = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

                            moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                            moveAmount.y -= descmoveAmount;

                            collisionInfo.descendingSlope = true;
                            collisionInfo.below = true;
                            collisionInfo.slopeAngle = slopeAngle;
                        }
                    }
                }
            }
        }

        void ResetFallingThroughPlatform(){
            collisionInfo.fallingThroughPlatform = false;
        }

        public struct CollisionInfo {
            public bool above, below;
            public bool left, right;

            public bool climbingSlope;
            public float slopeAngle, slopeAngleOld;
            public bool descendingSlope;
            public bool fallingThroughPlatform;

            public void Reset() {
                above = below = left = right = false;
                climbingSlope = false;
                descendingSlope = false;

                slopeAngleOld = slopeAngle;
                slopeAngle = 0;
            }
        }

        #endregion
    }
}