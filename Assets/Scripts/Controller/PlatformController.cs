﻿using System;
using System.Collections.Generic;
using UnityEngine;

using  MetaRunner.Helper;

namespace MetaRunner.Controller {

    /// <summary>
    /// Platform controller controls the movement of platform.
    /// It also calculates the players movement standing on the platform.
    /// The physics of this game is raycast based, not Unity's rigidbody physics.
    /// In current game, I didn't use any moving platform, but this script supports that.    
    /// </summary>

    public class PlatformController : RaycastController {

        // Layer mask for filtering who can stand on the platform.
        public LayerMask passengerMask;
        public float speed = 1;
        
        // Add waypoints, platform will follow that path.
        public Vector3[] localWaypoints;
        
        // Loop movement.
        public bool cyclic;

        // adds non-linearity.
        [Range(0,2)]
        public float easeAmount = 1;

        // Calculation helper variables.
        Vector3[] globalWaypoints;
        float percentBetweenWaypoints = 0;
        int fromWaypointIndex = 0;

        // Keeps the reference of passengers standing on the platform. Depending of game type there can be multiple players standing on platform.
        Dictionary<Transform, PlayerController> passengerDictionary = new Dictionary<Transform, PlayerController>();
        List<PassengerMovement> passengerMovements;

        SetThemeColor setThemeColor;
        AudioSource audioSource;

        public override void Start() {
            base.Start();

            globalWaypoints = new Vector3[localWaypoints.Length];
            for(int i = 0; i < localWaypoints.Length; i++){
                globalWaypoints[i] = localWaypoints[i] + transform.position;
            }

             setThemeColor = GetComponent<SetThemeColor>();
             audioSource = GetComponent<AudioSource>();
        }

        void Update() {
            UpdateRaycastOrigins();

            Vector2 velocity = CalculatePlatformMovement();
            
            CalculatePassengerMovement(velocity);

            MovePassengers(true);
            transform.Translate(velocity);
            MovePassengers(false);
        }

        void OnTriggerEnter2D(Collider2D col){
            ChangePlatformColor(GameTheme.ColorLabel.PLATFORM_ACTIVE);
        }

        void OnTriggerExit2D(Collider2D col){
            ChangePlatformColor(GameTheme.ColorLabel.PLATFORM_NORMAL);
        }

        void ChangePlatformColor(GameTheme.ColorLabel cl){
            if(setThemeColor != null) setThemeColor.SetColor(cl);
        }

        void EnemyDied(){
            audioSource.Play();
        }

        float Ease(float x){
            float a = easeAmount + 1;
            float xa = Mathf.Pow(x, a);

            return xa / (xa + Mathf.Pow(1-x, a));
        }

        // Calculate the platform movement on waypoints.
        Vector2 CalculatePlatformMovement(){
            if(localWaypoints.Length == 0) return Vector2.zero;

            fromWaypointIndex = fromWaypointIndex % globalWaypoints.Length;
            int toWayPointIndex = (fromWaypointIndex + 1) % globalWaypoints.Length;

            Vector3 from = globalWaypoints[fromWaypointIndex];
            Vector3 to = globalWaypoints[toWayPointIndex];

            float distanceBetweenTwoWaypoints = Vector2.Distance(from, to);
            percentBetweenWaypoints += Time.deltaTime * speed / distanceBetweenTwoWaypoints;
            float easedPercent = Ease(Mathf.Clamp01(percentBetweenWaypoints));

            Vector3 newPos = Vector3.Lerp(from, to, easedPercent);

            if(percentBetweenWaypoints >= 1.0f){
                percentBetweenWaypoints = 0;
                fromWaypointIndex++;

                if(!cyclic){
                    if(fromWaypointIndex >= globalWaypoints.Length - 1){
                        fromWaypointIndex = 0;
                        System.Array.Reverse(globalWaypoints);
                    }
                }
            }

            return newPos - transform.position;
        }

        // moves the passenger standing platform.
        void MovePassengers(bool beforeMovePlatform){
            foreach(PassengerMovement passenger in passengerMovements){
                if(!passengerDictionary.ContainsKey(passenger.transform)){
                    passengerDictionary.Add(passenger.transform, passenger.transform.GetComponent<PlayerController>());
                }

                if(passenger.moveBeforePlatform == beforeMovePlatform){
                    passengerDictionary[passenger.transform].Move(passenger.velocity, passenger.standingOnPlatform);
                }
            }
        }

        // Calculates the passenger movement standing on platform.
        void CalculatePassengerMovement(Vector2 velocity) {            
            HashSet<Transform> movedPassengers = new HashSet<Transform>();
            passengerMovements = new List<PassengerMovement>();

            // check movement direction
            float directionX = Mathf.Sign(velocity.x);
            float directionY = Mathf.Sign(velocity.y);

            // calculate vertical movement
            if(velocity.y != 0) {
                // calculate ray length
                float rayLength = Math.Abs(velocity.y) + skinWidth;

                // loop through the rays
                for (int i = 0; i < verticalRayCount; i++) {

                    // calculate ray origin
                    Vector2 rayOrigin = directionY == -1 ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                    rayOrigin += verticalRaySpacing * i * Vector2.right;

                    // cast ray
                    RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passengerMask);
                    Debug.DrawRay(rayOrigin, Vector2.up * rayLength * directionY, Color.blue);

                    // if hit, adjust the target velocity
                    if (hit && hit.distance != 0) {
                        if (!movedPassengers.Contains(hit.transform)) {
                            movedPassengers.Add(hit.transform);

                            float pushX = directionY == 1 ? velocity.x : 0;
                            float pushY = velocity.y - (hit.distance - skinWidth) * directionY;
                            
                            passengerMovements.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), directionY == 1, true));
                        }
                    }
                }
            }

            // Calculate horizontal movement. Pushes the player.
            if (velocity.x != 0) {
                float rayLength = Mathf.Abs(velocity.x) + skinWidth;

                for (int i = 0; i < horizontalRayCount; i++) {
                    Vector2 rayOrigin = directionX == -1 ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                    rayOrigin += Vector2.up * i * horizontalRaySpacing;
                    
                    RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, passengerMask);
                    if (hit && hit.distance != 0) {
                        if (!movedPassengers.Contains(hit.transform)) {
                            movedPassengers.Add(hit.transform);
                            float pushX = velocity.x - (hit.distance - skinWidth) * directionX;
                            float pushY = -skinWidth;
                            
                            passengerMovements.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), false, true));
                        }
                    }
                }
            }

            // Passenger on horizontally or downward moving platform 
            if (directionY == -1 || (velocity.y == 0 && velocity.x != 0)) {
                float rayLength = skinWidth * 2;
                for (int i = 0; i < verticalRayCount; i++) {
                    Vector2 rayOrigin = raycastOrigins.topLeft + Vector2.right * i * verticalRaySpacing;
                    RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, passengerMask);
                    if (hit && hit.distance != 0) {
                        if (!movedPassengers.Contains(hit.transform)) {
                            movedPassengers.Add(hit.transform);

                            float pushX = velocity.x;
                            float pushY = velocity.y;
                            
                            passengerMovements.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), true, false));
                        }
                    }
                }
            }
        }

        // Holds passenger movement information
        struct PassengerMovement {
            public Transform transform;
            public Vector3 velocity;
            public bool standingOnPlatform;
            public bool moveBeforePlatform;

            public PassengerMovement(Transform _transform, Vector3 _velocity, bool _standingOnPlatform, bool _moveBeforePlatform) {
                transform = _transform;
                velocity = _velocity;
                standingOnPlatform = _standingOnPlatform;
                moveBeforePlatform = _moveBeforePlatform;
            }
        }

        void OnDrawGizmos(){
            if(localWaypoints != null){
                float size = 0.3f;

                for(int i = 0; i < localWaypoints.Length; i++){
                    Vector3 globalWaypoint = (Application.isPlaying) ? globalWaypoints[i] : localWaypoints[i] + transform.position;

                    Debug.DrawLine(globalWaypoint - Vector3.up * size, globalWaypoint + Vector3.up * size, Color.green);
                    Debug.DrawLine(globalWaypoint - Vector3.left * size, globalWaypoint + Vector3.left * size, Color.green);
                }
            }
        }
    }
}