﻿using UnityEngine;
using MetaRunner.Helper;

namespace MetaRunner.Controller {

    /// <summary>
    /// Controls the movement of enemy on platform.
    /// </summary>
    
    public class EnemyController : MonoBehaviour {

        // Speed of enemy.
        public float speed = 1f;

        // Half length of the platform it's standing.
        float platformLengthHalf;
        
        // ** Helper variables
        float reverse = -1f;
        Vector3 targetEnd;
        // **
        
        void Start() {
            platformLengthHalf = GetComponentInParent<SyncPlatformSettings>().size.x * 0.5f;
            targetEnd = new Vector3(platformLengthHalf * -1f, transform.localPosition.y, 0);

            transform.localPosition = new Vector3(Random.Range(-platformLengthHalf, platformLengthHalf), transform.localPosition.y, 0);
        }

        void Update() {
            MoveEnemy();
        }

        void MoveEnemy(){
            Vector3 moveAmount = Vector3.right * Time.deltaTime * speed;

            if(Vector3.Distance(targetEnd, transform.localPosition) <= 0.25f) {
                targetEnd.x *= reverse;
                speed *= reverse;
            }

            transform.Translate(moveAmount, Space.Self);
        }

        // Destroy the missile and itself on hit.
        void OnTriggerEnter2D(Collider2D other){
            // Debug.Log(other.name);
            if(other.tag == "Missile"){
                FindObjectOfType<MetaRunner.Game.LevelManager>().extraScore += 20;
                SendMessageUpwards("EnemyDied", SendMessageOptions.DontRequireReceiver);
                
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}