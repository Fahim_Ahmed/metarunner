﻿using UnityEngine;
using UnityEngine.Events;

using MetaRunner.Helper;

namespace MetaRunner.Game {

    /// <summary>
    /// Theme manager.false Assign colors from theme template.
    /// Game changes its' theme randomly on every start.
    /// </summary>

    public class GameThemeManager : MonoBehaviour {

        [HideInInspector]
        public GameTheme currentTheme;

        public GameTheme[] themes;

        [HideInInspector]
        public UnityEvent triggerThemeChange;

        void Awake() {
            triggerThemeChange = new UnityEvent();            
            currentTheme = themes[0];
        }

        void Start(){
            NewTheme();
        }

        internal void NewTheme() {
            currentTheme = themes[Random.Range(0, themes.Length)];
            triggerThemeChange.Invoke();
        }
    }
}