﻿using UnityEngine;
using UnityEngine.Events;

namespace MetaRunner.Controller {
    /// <summary>
    /// This is the script for chasing red line.false
    /// Player will die if it touches.
    /// Also, it cleans up the generated platforms (destroys them on exit.)
    /// </summary>
    
    public class DestroyerController : MonoBehaviour {
        
        // Set the speed of destroyer.
        public float speed = 4f;

        // Invoke an event on hit with player.
        [HideInInspector]
        public UnityEvent onDestroyerHit;

        void Awake() {
            onDestroyerHit = new UnityEvent();            
        }

        void FixedUpdate() {
            float moveAmount = speed * Time.deltaTime;
            transform.Translate(Vector3.right * moveAmount);
        }

        void OnTriggerEnter2D(Collider2D col) {
            if(col.gameObject.tag == "Player"){
                onDestroyerHit.Invoke();
            }
        }

        void OnTriggerExit2D(Collider2D col) {
            PlatformController pc = col.gameObject.GetComponent<PlatformController>();
            if(pc != null && pc.gameObject.tag != "InitialPlatform") {
                Destroy(pc.gameObject);
            }
        }
    }
}