﻿using UnityEngine;

namespace MetaRunner.Controller {
    
    /// <summary>
    /// Creates rays around the bound of the object 
    /// and updates them.
    /// This game uses raycasting based physics.
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D))]
    public class RaycastController : MonoBehaviour {

        public LayerMask collisionMask;
        const float dstBetweenRays = 0.25f;

        [HideInInspector]
        public int horizontalRayCount;

        [HideInInspector]
        public int verticalRayCount;

        [HideInInspector]
        public float horizontalRaySpacing;
        [HideInInspector]
        public float verticalRaySpacing;        

        [HideInInspector]
        public new BoxCollider2D collider;

        [HideInInspector]
        public RaycastOrigins raycastOrigins;

        public const float skinWidth = 0.015f;

        public virtual void Awake(){
            collider = GetComponent<BoxCollider2D>();
        }

        public virtual void Start() {            
            CalculateRaySpacing();
        }

        #region raycast info
        public void UpdateRaycastOrigins() {
            Bounds bounds = GetComponent<BoxCollider2D>().bounds;
            bounds.Expand(skinWidth * -2f);

            raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
        }

        /// <summary>Calculate the positions of the rays around the bound.</summary>
        void CalculateRaySpacing() {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2.0f);

            float boundWidth = bounds.size.x;
            float boundHeight = bounds.size.y;

            horizontalRayCount = Mathf.RoundToInt(boundHeight / dstBetweenRays);
            verticalRayCount = Mathf.RoundToInt(boundWidth / dstBetweenRays);

            horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
            verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
        }

        /// <summary>Store raycast origins</summary>
        public struct RaycastOrigins {
            public Vector2 bottomLeft, bottomRight;
            public Vector2 topLeft, topRight;
        }

        #endregion
    }
}