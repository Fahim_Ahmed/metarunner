﻿using System;
using System.Collections;
using UnityEngine;
using MetaRunner.Game;

namespace MetaRunner.Helper{

    public class SetThemeColor : MonoBehaviour {
        public GameTheme.ColorLabel colorLabel;
        GameTheme theme;

        SpriteRenderer spriteRenderer;
        SkinnedMeshRenderer meshRenderer;

        Color activeColor;
        Color targetColor;
        
        [HideInInspector]
        public GameThemeManager gameThemeManager;

        void Awake(){
            gameThemeManager =  GameObject.FindObjectOfType<GameThemeManager>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        void Start(){
            // gameThemeManager =  GameObject.FindObjectOfType<GameThemeManager>();
            // spriteRenderer = GetComponent<SpriteRenderer>();

            // gameThemeManager = GameObject.FindGameObjectWithTag("ThemeManager").GetComponent<GameThemeManager>();
            ApplyTheme();
            gameThemeManager.triggerThemeChange.AddListener(ApplyTheme);
        }
        
        void ApplyTheme(){
            theme = gameThemeManager.currentTheme;
            activeColor = theme.GetColorFor(colorLabel);            

            if(spriteRenderer == null) {
                meshRenderer = GetComponent<SkinnedMeshRenderer>();
                meshRenderer.material.SetColor("_BaseColor", activeColor);
            }
            else {
                spriteRenderer.color = activeColor;
            }
        }

        internal void SetColor(GameTheme.ColorLabel cl){
            targetColor = gameThemeManager.currentTheme.GetColorFor(cl);
            StartCoroutine(LerpColor());
        }

        IEnumerator LerpColor(){
            float t = 0;
            while(true){
                activeColor = Color.Lerp(activeColor, targetColor, t);
                spriteRenderer.color = activeColor;
                t += Time.deltaTime;
                yield return new WaitForEndOfFrame();

                if(t >= 1f) break;
            }
        }

    }
}