using UnityEngine;

namespace MetaRunner.Helper {

    /// <summary>
    /// Important game units.
    ///</summary>

    public class GameUnits {
        public const float baseUnit = 0.675f;
        public const int maxWidthOfEachModule = 12;
        public const float actualMaxWidth = 12f * baseUnit;
    }
}