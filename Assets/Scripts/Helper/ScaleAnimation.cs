﻿using UnityEngine;

namespace MetaRunner.Helper{

    public class ScaleAnimation : MonoBehaviour {

        public float minScale = 0.75f;
        public float maxScale = 1.0f;

        public float scaleSpeed = 1f;

        float t = 0;

        void Update() {
            float c = Mathf.Sin(t);
            t += Time.deltaTime * scaleSpeed;

            if(t >= Mathf.PI) t = 0;
            
            transform.localScale = Vector3.one * RemapValue(c, 0, 1f, minScale, maxScale, true);

        }

        float RemapValue(float value, float start1, float stop1, float start2, float stop2, bool clamp = false) {
            float v = start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));

            if (clamp) {
                if (v < start2) v = start2;
                if (v > stop2) v = stop2;
            }

            return v;
        }
    }
}