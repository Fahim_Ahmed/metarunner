using UnityEngine;
using MetaRunner.Helper;

namespace MetaRunner.Helper {

    /// <summary>
    /// Base class for platform object.
    ///</summary>

    [CreateAssetMenu(fileName = "Color Palette", menuName = "Game Helpers/Game Theme")]
    public class GameTheme : ScriptableObject {
        public enum ColorLabel {
            PLAYER,
            PLAYER_FOCUS_OUT,
            PLAYER_FOCUS_IN,
            PLATFORM_NORMAL,
            PLATFORM_ACTIVE,
            BACKGROUND,
            ENEMY_EYE,
            ENEMY_BODY,
            MISSILE
        };

        public Color playerColor;
        public Color playerFocusCircleOut;
        public Color playerFocusCircleIn;
        public Color platformColorNormal;
        public Color platformColorActive;
        public Color backgroundColor;
        public Color enemyBodyColor;
        public Color enemyEyeColor;
        public Color missileColor;

        public Color GetColorFor(ColorLabel cl){
            switch(cl){
                case ColorLabel.PLAYER: return playerColor;
                case ColorLabel.PLAYER_FOCUS_IN: return playerFocusCircleIn;
                case ColorLabel.PLAYER_FOCUS_OUT: return playerFocusCircleOut;
                case ColorLabel.PLATFORM_ACTIVE: return platformColorActive;
                case ColorLabel.PLATFORM_NORMAL: return platformColorNormal;
                case ColorLabel.BACKGROUND: return backgroundColor;
                case ColorLabel.ENEMY_EYE: return enemyEyeColor;
                case ColorLabel.ENEMY_BODY: return enemyBodyColor;
                case ColorLabel.MISSILE: return missileColor;
            }

            return Color.white;
        }
    }
}