﻿using UnityEngine;
using MetaRunner.Props;
using MetaRunner.Controller;

namespace MetaRunner.Helper {

    /// <summary>
    /// Sync sprite size and collider size.
    /// Cannot use transform size because of sliced sprite.
    ///</summary>

    [ExecuteInEditMode]
    public class SyncPlatformSettings : MonoBehaviour {

        [HideInInspector]
        public Vector2 size;
        public int positionTopDown;
        public int positionLeftRight;
        SpriteRenderer spriteRenderer;
        BoxCollider2D collider2D;

        public PlatformTemplate platform;

        [HideInInspector]
        public GameObject enemyObject;

        void Awake() {
            enemyObject = gameObject.GetComponentInChildren<EnemyController>().gameObject;
            enemyObject.SetActive(false);
        }
        
        public void Start() {
            spriteRenderer = GetComponent<SpriteRenderer>();
            collider2D = GetComponent<BoxCollider2D>();            

            size = platform.GetSize();
            positionTopDown = platform.positionTopDown;
            positionLeftRight = platform.positionLeftRight;

            SyncSize();
            SyncPosition();
            gameObject.tag = platform.passable ? "Through" : "Untagged";
        }

        public void SyncSize(){
            spriteRenderer.size = platform.GetSize();
            collider2D.size = spriteRenderer.size;
        }

        public void SyncPosition(){
            Vector2 size = platform.GetSize();
            Vector2 curr_pos = transform.position;

            int positionTopDown = platform.positionTopDown;            
            int positionLeftRight = platform.positionLeftRight;
            
            if(!platform.isFreeSpawn){
                curr_pos = transform.localPosition;                

                if(positionLeftRight * PlatformTemplate.baseSize <= PlatformTemplate.maxSize - size.x)
                    curr_pos.x = (-PlatformTemplate.maxSize + size.x) * 0.5f + positionLeftRight * PlatformTemplate.baseSize;
                
                curr_pos.y = positionTopDown * PlatformTemplate.baseSize;
                
                transform.localPosition = curr_pos;
            }
            else {
                curr_pos.x = positionLeftRight;
                curr_pos.y = positionTopDown * PlatformTemplate.baseSize;
                transform.position = curr_pos;
            }
        }

        void Update(){
            if(!Application.isPlaying){
                SyncSize();
                SyncPosition();
            }
        }
    }
}